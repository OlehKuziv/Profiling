﻿using System.Security.Cryptography;

public class Program
{
    private static readonly Random random = new Random();
    public static string GeneratePasswordHashUsingSalt(string passwordText, byte[] salt)
    {

        var iterate = 100000;
        using var pbkdf2 = new Rfc2898DeriveBytes(passwordText, salt, iterate);
        byte[] hash = pbkdf2.GetBytes(20); // I dont see how to make this run faster

        byte[] hashBytes = new byte[36];
        Array.Copy(salt, 0, hashBytes, 0, 16);
        Array.Copy(hash, 0, hashBytes, 16, 20);

        var passwordHash = Convert.ToBase64String(hashBytes);

        return passwordHash;

    }

    public static byte[] GetNextSalt()
    {
        byte[] salt = new byte[16];
        random.NextBytes(salt);
        return salt;
    }

    static async Task Main(string[] args)
    {
        byte[] salt = GetNextSalt();
        string hashed = GeneratePasswordHashUsingSalt("Test@1234", salt);
        Console.WriteLine(hashed);
    }
}