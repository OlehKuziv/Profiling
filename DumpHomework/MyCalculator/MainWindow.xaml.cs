﻿// Decompiled with JetBrains decompiler
// Type: MyCalculatorv1.MainWindow
// Assembly: MyCalculatorv1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E4247A5-25E4-47A6-84F4-A414933F7536
// Assembly location: C:\Users\UX503388\Downloads\DumpHomework\DumpHomework\MyCalculator.exe

using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;


namespace MyCalculatorv1
{
    public partial class MainWindow : Window, IComponentConnector
    {
        /*internal TextBox tb;

        private bool _contentLoaded;

        public MainWindow()
        {
            InitializeComponent();
        }*/

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            tb.Text += button.Content.ToString();
        }

        private void Result_click(object sender, RoutedEventArgs e)
        {
            result();
        }

        private bool ArgumentsValid(string text)
        {
            string regexPattern = "^(\\d+)[\\+\\/\\*\\-]{1}\\d+$";
            Regex regex = new Regex(regexPattern);

            return regex.IsMatch(text);
        }


        private CalculatorOperands ParseIntoOperands(string text)
        {
            char[] action = new[]
            {
                '+', '-', '/', '*'
            };
            int actionIndex = text.IndexOfAny(action);
            if (actionIndex == -1)
            {
                tb.Text = "Invalid input";
                throw new Exception("Check validation rules");
            }
            string operand = tb.Text.Substring(actionIndex, 1);
            double numberOne = Convert.ToDouble(tb.Text.Substring(0, actionIndex));
            double numberTwo = Convert.ToDouble(tb.Text.Substring(actionIndex + 1, tb.Text.Length - actionIndex - 1));
            return new CalculatorOperands(numberOne, numberTwo, operand);
        }

        private void result()
        {
            if (!ArgumentsValid(tb.Text))
            {
                tb.Text = "Invalid input";
                return;
            }
            CalculatorOperands operands = ParseIntoOperands(tb.Text);

            tb.Text += "=" + Calculate(operands);
        }



        private double Calculate(CalculatorOperands operands)
        {
            switch (operands.Operand)
            {
                case "+":
                    {
                        return operands.NumberOne + operands.NumberTwo;
                    }
                case "-":
                    {
                        return operands.NumberOne - operands.NumberTwo;
                    }
                case "*":
                    {
                        return operands.NumberOne * operands.NumberTwo;
                    }
                case "/":
                    {
                        return operands.NumberOne / operands.NumberTwo;
                    }
                default: throw new ArgumentException("Unknown oparator. Check Validation code");
            }

        }
        private void Off_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Del_Click(object sender, RoutedEventArgs e)
        {
            tb.Text = "";
        }

        private void R_Click(object sender, RoutedEventArgs e)
        {
            if (tb.Text.Length > 0)
            {
                tb.Text = tb.Text.Substring(0, tb.Text.Length - 1);
            }
        }



















        // I DONT KNOW WHY SHOULD I COMMENT THIS OUT IN ORDER FOR PROJECT TO WORK. IT AUTOGENERATES SOME FILE WHICH HAS THESE METHODS
        /*public void InitializeComponent()
        {
            if (!_contentLoaded)
            {
                _contentLoaded = true;
                Uri resourceLocator = new Uri("/MyCalculator;component/mainwindow.xaml", UriKind.Relative);
                Application.LoadComponent(this, resourceLocator);
            }
        }

        void IComponentConnector.Connect(int connectionId, object target)
        {
            switch (connectionId)
            {
                case 1:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 2:
                    tb = (TextBox)target;
                    break;
                case 3:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 4:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 5:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 6:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 7:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 8:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 9:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 10:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 11:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 12:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 13:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 14:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 15:
                    ((Button)target).Click += Result_click;
                    break;
                case 16:
                    ((Button)target).Click += Button_Click_1;
                    break;
                case 17:
                    ((Button)target).Click += Off_Click_1;
                    break;
                case 18:
                    ((Button)target).Click += Del_Click;
                    break;
                case 19:
                    ((Button)target).Click += R_Click;
                    break;
                default:
                    _contentLoaded = true;
                    break;
            }*/
    }
}

