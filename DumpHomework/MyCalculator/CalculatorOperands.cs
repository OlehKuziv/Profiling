﻿namespace MyCalculatorv1
{
    struct CalculatorOperands
    {
        internal double NumberOne;

        internal double NumberTwo;
        internal string Operand;

        public CalculatorOperands(double numberOne, double numberTwo, string operand)
        {
            NumberOne = numberOne;
            NumberTwo = numberTwo;
            Operand = operand;
        }
    }
}
